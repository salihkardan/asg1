# Assignment 1 - Salih Kardan

In this homework, I did not use the sandbox provided by HortonWorks. Instead I setup my own single node hadoop cluster on my linux machine and the version of Hadoop was 1.2.1. It is a bit old, but it still works and does its job properly :) 

# Dependecies
   - maven
   - java 
   - ssh 

Here are the steps that you need during test of this project:

   - git clone https://sakardan@bitbucket.org/sakardan/asg1.git
   - mvn clean install 
   - copy "bigdata2016s-0.1.0-SNAPSHOT.jar" jar file under *target* directory to hadoop home directory which is /opt/hadoop-1.2.1 in my case. 
   - Create a directory on hadoop file system with the following command: `hadoop dfs -mkdir /user/root/`
   - Copy the pg100.txt file to hdfs with following command : `hadoop dfs -copyFromLocal pg100.txt /user/root/`
   - Execute the following command: `./hadoop jar ../bigdata2016s-0.1.0-SNAPSHOT.jar asg1.WordCount -input /user/root/pg100.txt -output /user/root/output`
   - Go to `http://localhost:50070/dfshealth.jsp` and  browser hdfs, you will see results under /user/root/output directory. 